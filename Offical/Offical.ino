#include <Servo.h>
#include "NewPing.h"
#include "UltraCar_IO.h"
#include "UltraCar_Direction.h"
Servo myServo;

int lookRight() {
  myServo.write(10);
  delay(500);
  int distance = readPing();
  delay(100);
  myServo.write(90);
  return distance;
}

int lookLeft() {
  myServo.write(170);
  delay(500);
  int distance = readPing();
  delay(100);
  myServo.write(90);
  return distance;
  delay(100);
}

int readPing() {
  delay(70);
  int cm = sonar.ping_cm();
  if (cm == 0) {
    cm = 250;
  }
  return cm;
}

void setup() {
  Serial.begin(115200);
  myServo.attach(_SERVO);
  myServo.write(90);
  delay(500);
  distance = readPing();
  delay(100);
  distance = readPing();
  delay(100);
  distance = readPing();
  delay(100);
  distance = readPing();
  delay(100);
  pinMode(IN_1, OUTPUT);
  pinMode(IN_2, OUTPUT);
  pinMode(IN_3, OUTPUT);
  pinMode(IN_4, OUTPUT);
  delay(10);
}


void loop() {
  if (!startA) {
    startA = true;
    randomStart = random(1, 2);
    callRandomStart(randomStart);
    delay(50);
  }
    int distanceRight = 0;
    int distanceLeft = 0;
    delay(50);

  if (distance <= 40) {
    stopRB();
    delay(100);
    goBB();
    delay(random(100, 200));
    stopRB();
    delay(50);
    distanceRight = lookRight();
    delay(300);
    distanceLeft = lookLeft();
    delay(300);

    if (distance >= distanceLeft) {
      goRandomRight = random(1, 2);
      callRandomRight(goRandomRight);
      stopRB();
    }
    else {
      goRandomLeft = random(1, 2);
      callRandomLeft(goRandomLeft);
      stopRB();
    }
  }
  else {
    goFF();
    Serial.println(distance);
  }
  distance = readPing();
  Serial.println(distance);
}

void callRandomLeft(int goRandomLeft) {
  if (goRandomLeft == 1) {
    goBL();
    delay(random(300, 600));
    goL();
    delay(random(300, 600));
  }
  if (goRandomLeft == 2) {
    goL();
    delay(random(300, 600));
    goBL();
    delay(random(300, 600));
  }
}

void callRandomRight(int goRandomRight) {
  if (goRandomRight == 1) {
    goBR();
    delay(random(300, 600));
    goR();
    delay(random(300, 600));
  }
  if (goRandomRight == 2) {
    goR();
    delay(random(300, 600));
    goBR();
    delay(random(300, 600));
  }
}

void callRandomStart(int randomStart) {
  if (randomStart == 1) {
    goR();
    delay(500);
    goL();
    delay(500);
    goFF();
  }
  if (randomStart == 2) {
    goBL();
    delay(500);
    goBR();
    delay(500);
    goFF();
  }
}
