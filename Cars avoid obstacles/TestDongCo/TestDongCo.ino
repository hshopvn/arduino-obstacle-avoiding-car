#define IN_1 4
#define IN_2 5
#define IN_3 6
#define IN_4 7

int speedCar = 220;
int speedCar_half = 2;
int speedCar_th = 160;


void setup() {
  pinMode(IN_1, OUTPUT);
  pinMode(IN_2, OUTPUT);
  pinMode(IN_3, OUTPUT);
  pinMode(IN_4, OUTPUT);
}

void loop() {
  goFF();
  delay(1000);
  stopRB();
  delay(1000);

  goBB();
  delay(1000);
  stopRB();
  delay(1000);

  goR();
  delay(2000);
  stopRB();
  delay(1000);

  goL();
  delay(1000);
  stopRB();
  delay(1000);

  goL();
  delay(1000);
  stopRB();
  delay(1000);

}

void goFF() {
  analogWrite(IN_1, speedCar_th);
  analogWrite(IN_2, 0);
  analogWrite(IN_3, speedCar_th);
  analogWrite(IN_4, 0);
}
void goBB() {
  analogWrite(IN_1, 0);
  analogWrite(IN_2, speedCar_th);
  analogWrite(IN_3, 0);
  analogWrite(IN_4, speedCar_th);
}

void goR() {
  analogWrite(IN_1, speedCar);
  analogWrite(IN_2, 0);
  analogWrite(IN_3, 0);
  analogWrite(IN_4, speedCar);
}
void goL() {
  analogWrite(IN_1, 0);
  analogWrite(IN_2, speedCar);
  analogWrite(IN_3, speedCar);
  analogWrite(IN_4, 0);
}
void goBR() {

  analogWrite(IN_1, 0);
  analogWrite(IN_2, speedCar);
  analogWrite(IN_3, 0);
  analogWrite(IN_4, speedCar / speedCar_half);
}


void goBL() {
  analogWrite(IN_1, 0);
  analogWrite(IN_2, speedCar / speedCar_half);
  analogWrite(IN_3, 0);
  analogWrite(IN_4, speedCar);
}


void stopRB() {
  analogWrite(IN_1, 0);
  analogWrite(IN_2, 0);
  analogWrite(IN_3, 0);
  analogWrite(IN_4, 0);
}
